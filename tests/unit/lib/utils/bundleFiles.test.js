const bundleFiles = require('../../../../lib/utils/bundleFiles');
const Graph = require('../../../../lib/utils/Graph');

describe('Utils | bundleFiles', () => {
    it('should throw an error if graph is not provided', () => {
        expect(() => {
            bundleFiles();
        }).toThrowError('graph is not a Graph');
    });

    it('should throw an error if entry path is incorrect', () => {
        const graph = new Graph();

        expect(() => {
            bundleFiles(graph, 'a');
        }).toThrowError('entryNode doesnt exist');
    });


    it('should bundle correct file', () => {
        const graph = new Graph();

        graph.addVertex({filename: 'a', code: 'a code' });
        graph.addVertex({filename: 'b', code: 'b code'});
        graph.addVertex({filename: 'c', code: 'c code'});

        graph.addEdge('a', 'b');
        graph.addEdge('b', 'c');

        const result = bundleFiles(graph, 'a');

        expect(result).toBe(`const __farFetchModules = {};
const __farFetchRequire = (path) => {
    return __farFetchModules[path]();
};__farFetchModules['c'] = function () {
c code
};__farFetchModules['b'] = function () {
b code
};a code`);
    });
});

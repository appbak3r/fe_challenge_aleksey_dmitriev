const Graph = require('../../../../lib/utils/Graph');

describe('utils | Graph', () => {
    it('should create an empty Graph object', () => {
        const graph = new Graph();

        expect(graph.constructor.name).toBe('Graph');
        expect(graph.vertices).toStrictEqual({});
    });

    it('should throw an error if passed value  to addVertex is not filedata', () => {
        const graph = new Graph();

        expect(() => {
            graph.addVertex("not file data")
        }).toThrowError('fileData is invalid');
    });

    it('should add new vertex as as GraphNode instance', () => {
        const graph = new Graph();
        const vertex = {filename: 'a'};

        graph.addVertex(vertex);

        expect(graph.vertices['a'].constructor.name).toBe('GraphNode');

        expect(graph.vertices['a'].value).toBe(vertex.filename);
        expect(graph.vertices['a'].fileData).toBe(vertex);
    });

    it('should not override vertex', () => {
        const graph = new Graph();
        const vertex = {filename: 'a'};
        const vertex2 = {filename: 'a'};

        graph.addVertex(vertex);

        expect(() => {
            graph.addVertex(vertex2);
        }).toThrow();
    });

    it('should add edge if vertices exist', () => {
        const graph = new Graph();
        const vertex = {filename: 'a'};
        const vertex2 = {filename: 'b'};

        graph.addVertex(vertex);
        graph.addVertex(vertex2);

        graph.addEdge('a', 'b');

        expect(graph.vertices[vertex.filename].edges.has(vertex2.filename)).toBeTruthy();
    });

    it('should not add edge if one of the vertices doesnt exist', () => {
        const graph = new Graph();

        graph.addVertex({filename: 'a'});


        expect(() => {
            graph.addEdge('a', 'b');
        }).toThrowError('Vertices dont exist');


        expect(() => {
            graph.addEdge('b', 'a');
        }).toThrowError('Vertices dont exist');
    });

    it('should has direct circular dependency', () => {
        const graph = new Graph();

        graph.addVertex({filename: 'a'});
        graph.addVertex({filename: 'b'});

        graph.addEdge('a', 'b');
        graph.addEdge('b', 'a');

        expect(graph.hasCircularDependency('a', 'b')).toBeTruthy();
    });

    it('should has circular dependency', () => {
        const graph = new Graph();

        graph.addVertex({filename: 'a'});
        graph.addVertex({filename: 'b'});
        graph.addVertex({filename: 'c'});

        graph.addEdge('a', 'b');
        graph.addEdge('b', 'c');

        expect(graph.hasCircularDependency('a', 'c')).toBeTruthy();
    });

    it('should not have circular dependency', () => {
        const graph = new Graph();

        graph.addVertex({filename: 'a'});
        graph.addVertex({filename: 'b'});
        graph.addVertex({filename: 'c'});

        graph.addEdge('a', 'b');
        graph.addEdge('b', 'c');

        expect(graph.hasCircularDependency('c', 'a')).toBeFalsy();
        expect(graph.hasCircularDependency('c', 'b')).toBeFalsy();
    });
});

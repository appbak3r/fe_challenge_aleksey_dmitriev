const parseFile = require('../../../../lib/utils/parseFile');

jest.mock('fs');
jest.mock('path');

describe('utils | parseFile', () => {
    const MOCK_FILE_INFO = {
        a: `
            const a = require('./a');
            const external = require('external');
            
            console.log(a);
            
            module.exports = a;
            module.exports = 'test';
        `,
        d: `some text file`,
    };

    beforeEach(() => {
        // Set up some mocked out file info before each test
        require('fs').__setMockFiles(MOCK_FILE_INFO);
    });

    it('should throw an error if file doesnt exist', () => {
        expect(() => {
            parseFile('b');
        }).toThrow();
    });

    it('should fail if not js code provided', () => {
        expect(() => {
            parseFile('d');
        }).toThrow();
    });

    it('should correctly replace code', () => {
        const result = parseFile('a');

        expect(result.code).toBe(`
            const a = __farFetchRequire("__dirname__./a.js");
            const external = require('external');
            
            console.log(a);
            
            ;
            return 'test';
        `);
    });

    it('should correctly collect dependencies', () => {
        const result = parseFile('a');

        expect(result.dependencies).toStrictEqual(['./a.js']);
    });
});

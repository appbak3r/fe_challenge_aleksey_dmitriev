const makeFileGraph = require('../../../../lib/utils/makeFileGraph');

jest.mock('fs');
jest.mock('path');

describe('utils | makeFileGraph', () => {
    const MOCK_FILE_INFO = {
        '__dirname__./a.js': `
            const external = require('external');
                        
            module.exports = 'a';
        `,
        '__dirname__./b.js': `            
            module.exports = 'b';
        `,
        '__dirname__./c.js': `
            const a = require('./a');
            const b = require('./b');
            
            console.log(a,b);
                        
            module.exports = 'test';
        `,
        './entry': `
            const c = require('./c');
                        
            console.log(c);
        `,
        '__dirname__./entryCircular.js': `
            const c = require('./c2');
                        
            console.log(c);
        `,
        '__dirname__./c2.js': `
            const entry = require('./entryCircular');
            
            console.log(entry);
        `,
        './d': `
            const external = require('external');
                        
            module.exports = 'test';
        `,
    };

    beforeEach(() => {
        // Set up some mocked out file info before each test
        require('fs').__setMockFiles(MOCK_FILE_INFO);
    });


    it('should return graph with 1 vertex ( no dependencies )', () => {
        const graph = makeFileGraph('./d');

        expect(Object.keys(graph.vertices).length).toBe(1);
    });

    it('should throw circular error', () => {

        expect(() => {
            makeFileGraph('__dirname__./entryCircular.js')
        }).toThrowError('Circular dependency');
    });

    it('should return correct graph', () => {
        const graph = makeFileGraph('./entry');

        expect(Object.keys(graph.vertices).length).toBe(4);

        expect(Array.from(graph.vertices['./entry'].edges)).toStrictEqual(['__dirname__./c.js']);
        expect(Array.from(graph.vertices['__dirname__./c.js'].edges)).toStrictEqual(['__dirname__./a.js', '__dirname__./b.js'])
        expect(Array.from(graph.vertices['__dirname__./b.js'].edges)).toStrictEqual([]);
        expect(Array.from(graph.vertices['__dirname__./a.js'].edges)).toStrictEqual([]);
    });
});

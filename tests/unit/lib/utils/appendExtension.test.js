const appendExtension = require('../../../../lib/utils/appendExtension');

describe("utils | appendExtension", () => {
    it('should accept only string', () => {
        expect(() => {
            appendExtension(1);
        }).toThrow();

        expect(() => {
            appendExtension([]);
        }).toThrow();

        expect(() => {
            appendExtension({});
        }).toThrow();

        expect(() => {
            appendExtension(false);
        }).toThrow();
    });

    it('should append .js extension to string', () => {
        const str = 'filename';

        expect(appendExtension(str)).toBe('filename.js');
    });

    it('should not append .js extension to string', () => {
        const str = 'filename.js';

        expect(appendExtension(str)).toBe('filename.js');
    });
});

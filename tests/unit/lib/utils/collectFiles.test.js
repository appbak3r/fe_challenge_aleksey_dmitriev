const collectFiles = require('../../../../lib/utils/collectFiles');
const Graph = require('../../../../lib/utils/Graph');

describe('utils | collectFiles', () => {
    it('should throw if wrong arguments are passed', () => {
        expect(() => {
            collectFiles();
        }).toThrow();

        expect(() => {
            collectFiles('test', 'test');
        }).toThrow();

        expect(() => {
            collectFiles(new Graph(), 'test');
        }).toThrow();
    });

    it('should return empty array for empty graph', () => {
        const graph = new Graph();
        graph.addVertex({filename: 'a'});

        expect(collectFiles(graph, graph.vertices['a'])).toStrictEqual([]);
    });

    it('should return array of graph edge values', () => {
        const graph = new Graph();

        graph.addVertex({filename: 'a'});
        graph.addVertex({filename: 'b'});
        graph.addVertex({filename: 'c'});

        graph.addEdge('a', 'b');
        graph.addEdge('a', 'c');
        graph.addEdge('c', 'b');

        expect(collectFiles(graph, graph.vertices['a'])).toStrictEqual(['b','c','b']);
    });
});

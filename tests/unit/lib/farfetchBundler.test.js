const makeFileGraph = require('../../../lib/utils/makeFileGraph');
const bundleFiles = require('../../../lib/utils/bundleFiles');
const fs = require('fs');

jest.mock('../../../lib/utils/makeFileGraph', );
jest.mock('../../../lib/utils/bundleFiles');

jest.mock('fs');
jest.mock('path');

const farfetchBundler = require('../../../lib/farfetchBundler');

describe('farfetchBundler', () => {
    it('should throw an error if entryPath is not provided', () => {
        expect(() => {
            farfetchBundler([]);
        }).toThrowError('entryPath must be a string');
    });

    it('should call makeFileGraph', () => {
        farfetchBundler('a');

        expect(makeFileGraph).toHaveBeenCalled();
    });

    it('should call bundleFiles', () => {
        farfetchBundler('a');

        expect(bundleFiles).toHaveBeenCalled();
    });

    it('should write result to bundle.js', () => {
        farfetchBundler('a');

        expect(fs.writeFileSync).toHaveBeenCalledWith('__dirname__bundle.js', undefined);
    });
});

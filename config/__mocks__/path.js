'use strict';
const path = jest.genMockFromModule('path');


function dirname() {
    return '__dirname__';
}

function join(path1, path2) {
    return path1 + path2;
}

path.dirname = dirname;
path.join = join;

module.exports = path;

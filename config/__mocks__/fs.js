'use strict';

const fs = jest.genMockFromModule('fs');

let mockFiles = {};
function __setMockFiles(newMockFiles) {
    mockFiles = newMockFiles;
}


function existsSync(path) {
    return Boolean(mockFiles[path]);
}

function readFileSync(path) {
    return mockFiles[path];
}


fs.__setMockFiles = __setMockFiles;
fs.existsSync = existsSync;
fs.readFileSync = readFileSync;
fs.writeFileSync = jest.fn();

module.exports = fs;

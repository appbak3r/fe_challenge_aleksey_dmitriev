const farfetchBunlder = require('./lib/farfetchBundler');

const entry = process.argv[2];

if (entry) {
    farfetchBunlder(entry);
}

module.exports = farfetchBunlder;

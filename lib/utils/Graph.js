function GraphNode(value, fileData) {
    this.value = value;
    this.fileData = fileData;
    this.edges = new Set();
}

function Graph() {
    this.vertices = {};
}

Graph.prototype.addVertex = function (fileData) {
    if (!fileData || !fileData.filename) {
        throw new Error('fileData is invalid');
    }

    let value = fileData.filename;

    if (this.vertices[value]) {
        throw new Error('Trying to override vertex');
    }

    this.vertices[value] = new GraphNode(value, fileData);
};

Graph.prototype.addEdge = function (start, end) {
    if (!this.vertices[start] || !this.vertices[end]) {
        throw new Error('Vertices dont exist');
    }

    this.vertices[start].edges.add(end);
};

Graph.prototype.hasCircularDependency = function (childVertex, parentVertex) {
    if (!this.vertices[childVertex]) {
        throw new Error(`No vertex: ${childVertex}`);
    }

    // Check if child directly has edge with parent
    if (this.vertices[childVertex].edges.has(parentVertex)) {
        return true;
    }

    // Check if child vertices has edges with parent
    for (const edge of this.vertices[childVertex].edges) {
        if (this.vertices[edge].edges.has(parentVertex)) {
            return true;
        }
    }

    return false;
};

module.exports = Graph;

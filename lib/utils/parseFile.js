const jscodeshift = require('jscodeshift');
const fs = require('fs');
const path = require('path');
const appendExtension = require('./appendExtension');

/**
 * Build AST and check for dependencies. Also replaces code with local require function.
 *
 * @param filename
 * @returns {{filename: string, code: string, dependencies: Array}}
 */
function parseFile(filename) {
    if (!fs.existsSync(filename)) {
        throw new Error('file doesnt exist');
    }

    const dependencies = [];

    const fileContent = fs.readFileSync(filename, 'utf-8');

    const root = jscodeshift(fileContent);

    root.find(jscodeshift.CallExpression, {
        callee: {
            name: 'require'
        }
    }).replaceWith((nodePath) => {
        const {node} = nodePath;

        // Process only relative dependencies
        if (node.arguments[0].value[0] === '.') {
            const dependencyPath = appendExtension(node.arguments[0].value);

            dependencies.push(dependencyPath);

            node.callee.name = '__farFetchRequire';

            node.arguments[0].value = path.join(path.dirname(filename), appendExtension(node.arguments[0].value));
        }

        return node;
    });

    let moduleExportsNodes = 0;

    // Replace last module.exports with return, delete others
    root.find(jscodeshift.AssignmentExpression, {
        left: {
            type: 'MemberExpression',
            object: {
                name: 'module',
            },
            property: {
                name: 'exports'
            }
        }
    }).forEach(() => {
        moduleExportsNodes += 1;
    }).replaceWith(({node}) => {
        if (moduleExportsNodes === 1) {
            return `return ${jscodeshift(node.right).toSource()}`;
        } else {
            moduleExportsNodes -= 1;
            return '';
        }
    });


    return {
        filename,
        dependencies,
        code: root.toSource(),
    };
}

module.exports = parseFile;

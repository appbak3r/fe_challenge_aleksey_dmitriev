const path = require('path');
const Graph = require('./Graph');
const parseFile = require('./parseFile');

/**
 * Prepare graph starting from entry path
 *
 * @param {string} filePath
 * @returns {Graph}
 */
function makeFileGraph(filePath) {
    const fileData = parseFile(filePath);
    const graph = new Graph();

    graph.addVertex(fileData);

    const queue = [fileData];

    // Loop until all dependent files processed
    for (const file of queue) {
        file.dependencies.forEach((dependency) => {
            const dirname = path.dirname(file.filename);

            let depPath = path.join(dirname, dependency);

            if (!graph.vertices[depPath]) {
                const child = parseFile(depPath);

                graph.addVertex(child);

                queue.push(child);
            }

            if (graph.hasCircularDependency(depPath, file.filename)) {
                throw new Error('Circular dependency');
            }

            graph.addEdge(file.filename, depPath);
        });
    }


    return graph;
}

module.exports = makeFileGraph;

/**
 * Collects file paths from graph edges of each node
 *
 * @param {Graph} graph
 * @param {GraphNode }node
 * @returns {Array}
 */
function collectFiles(graph, node) {
    if (!graph || graph.constructor.name !== 'Graph') {
        throw new Error('graph is not specified');
    }

    if (!node || node.constructor.name !== 'GraphNode') {
        throw new Error('node is not specified');
    }

    let files = [];

    for (const edgeName of node.edges) {
        const childNode = graph.vertices[edgeName];

        files.push(childNode.value);

        files = [...files, ...collectFiles(graph, childNode)];
    }

    return files;
}

module.exports = collectFiles;

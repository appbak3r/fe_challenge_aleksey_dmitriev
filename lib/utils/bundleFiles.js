const collectFiles = require('./collectFiles');

/**
 * Bundles all the code into the only string
 *
 * @param {Graph} graph
 * @param {string} entryPath
 * @returns {string}
 */
function bundleFiles(graph, entryPath) {
    if (!graph ||  graph.constructor.name !== 'Graph') {
        throw new Error('graph is not a Graph')
    }

    const entryNode = graph.vertices[entryPath];

    if (!entryNode) {
        throw new Error('entryNode doesnt exist')
    }

    let result = `const __farFetchModules = {};
const __farFetchRequire = (path) => {
    return __farFetchModules[path]();
};`;

    const files = [entryNode.value, ...collectFiles(graph, entryNode)];

    const vertices = {...graph.vertices};

    // Loop files in reverse order to get correct code order
    for (let i = files.length - 1; i >= 0; i--) {
        const filename = files[i];

        let vertex = vertices[filename];

        if (vertex) {
            // If file is entry just insert the code without wrapping it
            if (filename === entryPath) {
                result += vertex.fileData.code;
            } else {
                result +=
                    `__farFetchModules['${filename}'] = function () {
${vertex.fileData.code}
};`;
            }


            // Remove file from graph vertices to not duplicate it in source results
            vertices[filename] = null;
        }
    }

    return result;
}

module.exports = bundleFiles;

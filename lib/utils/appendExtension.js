/**
 * Append .js extension to filename
 *
 * @param filename string
 * @returns {string}
 */
function appendExtension (filename) {
    if (typeof filename !== 'string') {
        throw new Error('filename should be a string');
    }

    const ext = filename.slice(filename.length - 3, filename.length);

    if (ext !== '.js') {
        return `${filename}.js`;
    }

    return filename;
}

module.exports = appendExtension;

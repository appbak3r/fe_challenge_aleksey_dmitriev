const fs = require('fs');
const path = require('path');
const makeFileGraph = require('./utils/makeFileGraph');
const bundleFiles = require('./utils/bundleFiles');

function farfetchBundler(entryPath) {
    if (typeof entryPath !== "string") {
        throw new Error('entryPath must be a string');
    }

    const graph = makeFileGraph(entryPath);

    const result = bundleFiles(graph, entryPath);

    const dest = path.dirname(entryPath);

    fs.writeFileSync(path.join(dest, 'bundle.js'), result);
}

module.exports = farfetchBundler;

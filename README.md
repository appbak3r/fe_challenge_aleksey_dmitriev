# FarFetch bundler for node.js
Zero configuration node.js application bundler.

### Getting started

1. Download repository and install dependencies

```
npm install
```

2. Bundle you application. Your code will appear in `bundle.js` file.

```
node index.js /path/to/your/entry.js
```

3. Don't forget to keep external node_modules with bundle.js

